<?php

/**
 * @file
 * Contains \Drupal\development_reports\Form\Overview.
 */

namespace Drupal\development_reports\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\development_reports\DevelopmentReportPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Cache\Cache;

/**
 * Provides the comments overview administration form.
 */
class Overview extends FormBase {

  /**
   * Plugin manager.
   *
   * @var DevelopmentReportPluginManager
   */
  protected $pluginManager;

  /**
   * Data formatter.
   *
   * @var DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * State API.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Creates the form.
   */
  public function __construct(DevelopmentReportPluginManager $plugin_manager, DateFormatterInterface $date_formatter, StateInterface $state) {
    $this->pluginManager = $plugin_manager;
    $this->dateFormatter = $date_formatter;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.development_report'),
      $container->get('date.formatter'),
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'development_reports_overview';
  }

  /**
   * Form constructor for the development reports overview administration form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $updated = $this->state->get('development_reports_updated');

    $form['options'] = [
      '#type' => 'details',
      '#title' => $this->t('Options'),
      '#open' => FALSE,
      '#attributes' => ['class' => ['container-inline']],
    ];

    $form['options']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Update selected reports'),
    ];

    $options = [];

    $header = [
      'name' => $this->t('Name'),
      'description' => $this->t('Description'),
      'changed' => $this->t('Updated'),
      'records' => $this->t('Records'),
    ];

    foreach ($this->pluginManager->getDefinitions() as $definition) {
      $id = $definition['id'];
      $plugin = $this->pluginManager->createInstance($id);
      $options[$definition['id']] = [
        'name' => [
          'data' => [
            '#type' => 'link',
            '#title' => $definition['label'],
            '#url' => Url::fromUri('internal:/admin/reports/development/' . $id),
          ],
        ],
        'description' => $definition['description'],
        'changed' => isset($updated[$id]) ? $this->dateFormatter->format($updated[$id]) : '',
        'records' => $plugin->countRecords(),
      ];
    }

    $form['reports'] = [
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $form_state->setValue('reports', array_filter($form_state->getValue('reports')));
    if (count($form_state->getValue('reports')) == 0) {
      $form_state->setErrorByName('', $this->t('Select one or more reports to perform the update on.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $updated = $this->state->get('development_reports_updated');

    foreach ($form_state->getValue('reports') as $report) {
      $plugin = $this->pluginManager->createInstance($report);
      $plugin->updateData();
      $updated[$report] = REQUEST_TIME;
    }

    $module_handler = \Drupal::service('module_handler');

    // @TODO: Update only views cache.
    $module_handler->invokeAll('cache_flush');
    foreach (Cache::getBins() as $cache_backend) {
      $cache_backend->deleteAll();
    }

    \Drupal::service('router.builder')->rebuild();
    $this->state->set('development_reports_updated', $updated);

    drupal_set_message($this->t('The update has been performed.'));
    $form_state->setRedirect('development_reports.overview');
  }

}
