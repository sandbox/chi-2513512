<?php

/**
 * @file
 * Contains \Drupal\development_report\DevelopmentReportInterface.
 */

namespace Drupal\development_reports;

/**
 * Interface definition for 'development report' plugins.
 */
interface DevelopmentReportInterface {

  /**
   * Recreates data in the plugin table.
   */
  public function updateData();

  /**
   * Defines the database schema for the plugin table.
   *
   * @see development_reports_schema()
   */
  public function schema();

  /**
   * Describes plugin table and fields to Views.
   *
   * @see development_reports_views_data()
   */
  public function viewsData();

  /**
   * Counts records in the plugin table.
   */
  public function countRecords();

}
