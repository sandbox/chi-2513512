<?php

/**
 * @file
 * Contains \Drupal\development_reports\Plugin\DevelopmentReport\DevelopmentReportPluginBase.
 */

namespace Drupal\development_reports;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for "Development report' plugins.
 */
abstract class DevelopmentReportPluginBase extends PluginBase implements DevelopmentReportInterface {

  /**
   * Collects data.
   *
   * @return array
   *   An associative array of collected data in which each key corresponds to
   *   to certain column in the plugin table.
   */
  abstract protected function collect();

  /**
   * {@inheritdoc}
   */
  public function updateData() {

    /** @var \Drupal\Core\Database\Connection $connection */
    $connection = \Drupal::service('database');

    $data = $this->collect();
    $table = 'development_reports_' . $this->getBaseId();

    $connection
      ->truncate($table)
      ->execute();
    $query = $connection
      ->insert($table)
      ->fields(array_keys($data[0]));
    foreach ($data as $record) {
      $query->values($record);
    }
    $query->execute();
    return $this;
  }

  /**
   * Returns views definition for a standard field.
   */
  protected function viewsStandardFeld($title, $help) {
    return [
      'title' => $title,
      'help' => $help,
      'field' => ['id' => 'standard'],
      'argument' => ['id' => 'string'],
      'filter' => ['id' => 'string'],
      'sort' => ['id' => 'standard'],
    ];
  }

  /**
   * Returns views definition for a boolean field.
   */
  protected function viewsBooleanField($title, $help, $label) {
    return [
      'title' => $title,
      'help' => $help,
      'field' => ['id' => 'boolean'],
      'filter' => [
        'id' => 'boolean',
        'label' => $label,
        'type' => 'yes-no',
        'accept null' => TRUE,
        'use_equal' => TRUE,
      ],
      'sort' => ['id' => 'standard'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function countRecords() {

    /** @var \Drupal\Core\Database\Connection $connection */
    $connection = \Drupal::service('database');

    return $connection->select('development_reports_' . $this->getBaseId())
      ->countQuery()
      ->execute()
      ->fetchField();
  }

}
