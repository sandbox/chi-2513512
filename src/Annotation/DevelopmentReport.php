<?php

/**
 * @file
 * Contains \Drupal\development_report\Anotation\DevelopmentReport.
 */

namespace Drupal\development_reports\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a development report annotation object.
 *
 * @Annotation
 */
class DevelopmentReport extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $title;

  /**
   * The description of the archiver plugin.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $description;

}
