<?php

/**
 * @file
 * Contains \Drupal\development_reports\Plugin\DevelopmentReport\Services.
 */

namespace Drupal\development_reports\Plugin\DevelopmentReport;

use Drupal\development_reports\DevelopmentReportPluginBase;

/**
 * Plugin implementation of the 'Routes' development report.
 *
 * @DevelopmentReport(
 *   id = "routes",
 *   label = @Translation("Routes"),
 *   description = @Translation("List of Drupal routes.")
 * )
 */
class Routes extends DevelopmentReportPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function collect() {

    $route_provider = \Drupal::service('router.route_provider');

    foreach ($route_provider->getAllRoutes() as $route) {
      $methods = $route->getMethods();
      $defaults = $route->getDefaults();
      $data[] = [
        'path' => $route->getPath(),
        'host' => $route->getHost(),
        'get_method' => (int) in_array('GET', $methods),
        'post_method' => (int) in_array('POST', $methods),
        'title' => isset($defaults['_title']) ? $defaults['_title'] : '',
        'form' => isset($defaults['_form']) ? $defaults['_form'] : '',
        'controller' => isset($defaults['_controller']) ? $defaults['_controller'] : '',
      ];
    }

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function schema() {
    return [
      'description' => 'Routes',
      'fields' => [
        'path' => [
          'type' => 'varchar',
          'length' => 128,
          'not null' => TRUE,
          'default' => '',
          'description' => 'Route path.',
        ],
        'host' => [
          'type' => 'varchar',
          'length' => 128,
          'not null' => TRUE,
          'default' => '',
          'description' => 'Route host.',
        ],
        'get_method' => [
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 0,
          'size' => 'tiny',
          'description' => 'Boolean indicating whether get method is allowed.',
        ],
        'post_method' => [
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 0,
          'size' => 'tiny',
          'description' => 'Boolean indicating whether post method is allowed.',
        ],
        'title' => [
          'type' => 'varchar',
          'length' => 128,
          'not null' => TRUE,
          'default' => '',
          'description' => 'Route title.',
        ],
        'form' => [
          'type' => 'varchar',
          'length' => 128,
          'not null' => TRUE,
          'default' => '',
          'description' => 'Route form.',
        ],
        'controller' => [
          'type' => 'varchar',
          'length' => 128,
          'not null' => TRUE,
          'default' => '',
          'description' => 'Route controller',
        ],
        // @TODO: Support other route properties.
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function viewsData() {
    $data['table']['group'] = t('Routes');
    $data['table']['base'] = [
      'field' => 'path',
      'title' => t('Routes'),
      'help' => t('Contains a list of Drupal routes.'),
    ];
    $data['path'] = $this->viewsStandardFeld(t('Path'), t('Route path'));
    $data['host'] = $this->viewsStandardFeld(t('Host'), t('Route host.'));
    $data['get_method'] = $this->viewsBooleanField(
      t('GET'),
      t('Whether GET method is allowed for the route.'),
      t('GET is allowed')
    );
    $data['post_method'] = $this->viewsBooleanField(
      t('POST'),
      t('Whether POST method is allowed for the route.'),
      t('POST is allowed')
    );
    $data['title'] = $this->viewsStandardFeld(t('Title'), t('Route title.'));
    $data['form'] = $this->viewsStandardFeld(t('Form'), t('Route form.'));
    $data['controller'] = $this->viewsStandardFeld(t('Controller'), t('Route controller.'));

    return $data;
  }

}
