<?php

/**
 * @file
 * Contains \Drupal\development_reports\Plugin\DevelopmentReport\Services.
 */

namespace Drupal\development_reports\Plugin\DevelopmentReport;

use Drupal\development_reports\DevelopmentReportPluginBase;

/**
 * Plugin implementation of the 'Services' development report.
 *
 * @DevelopmentReport(
 *   id = "services",
 *   label = @Translation("Services"),
 *   description = @Translation("List of Drupal services.")
 * )
 */
class Services extends DevelopmentReportPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function collect() {

    /** @var \Drupal\Core\DependencyInjection\ContainerBuilder $container */
    $container = \Drupal::getContainer();
    $data = [];
    foreach ($container->getServiceIds() as $service_id) {
      $service = $container->get($service_id);
      $extension = '';
      if (is_object($service)) {
        $class = get_class($service);
        $class_items = explode('\\', $class);
        if ($class_items[0] == 'Drupal' && isset($class_items[1])) {
          $extension = $class_items[1];
        }
      }

      $data[] = [
        'id' => $service_id,
        'class' => is_object($service) ? get_class($service) : '',
        'extension' => $extension,
      ];
    }

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function schema() {
    return [
      'description' => 'Services',
      'fields' => [
        'id' => [
          'type' => 'varchar',
          'length' => 128,
          'not null' => TRUE,
          'default' => '',
          'description' => 'Service ID.',
        ],
        'class' => [
          'type' => 'varchar',
          'length' => 128,
          'not null' => TRUE,
          'default' => '',
          'description' => 'Service class.',
        ],
        'extension' => [
          'type' => 'varchar',
          'length' => 128,
          'not null' => TRUE,
          'default' => '',
          'description' => 'Service extension.',
        ],
      ],
      'primary key' => ['id'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function viewsData() {
    $data['table']['group'] = t('Services');
    $data['table']['base'] = [
      'field' => 'id',
      'title' => t('Services'),
      'help' => t('Contains a Drupal services.'),
    ];
    $data['id'] = $this->viewsStandardFeld(t('ID'), t('Service ID.'));
    $data['class'] = $this->viewsStandardFeld(t('Class'), t('Service class.'));
    $data['extension'] = $this->viewsStandardFeld(t('Extension'), t('Service extension.'));

    return $data;
  }

}
