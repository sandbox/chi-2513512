<?php

/**
 * @file
 * Contains \Drupal\development_reports\Plugin\DevelopmentReport\Libraries.
 */

namespace Drupal\development_reports\Plugin\DevelopmentReport;

use Drupal\development_reports\DevelopmentReportPluginBase;
use Drupal\Core\Extension\ExtensionDiscovery;

/**
 * Plugin implementation of the 'Libraries' development report.
 *
 * @DevelopmentReport(
 *   id = "libraries",
 *   label = @Translation("Libraries"),
 *   description = @Translation("List of Drupal libraries.")
 * )
 */
class Libraries extends DevelopmentReportPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function collect() {
    $discovery = \Drupal::service('library.discovery');

    $module_handler = \Drupal::service('module_handler');
    $theme_handler = \Drupal::service('theme_handler');

    $listing = new ExtensionDiscovery(DRUPAL_ROOT);
    $listing->setProfileDirectories([]);
    $extensions = $listing->scan('module', TRUE);
    $extensions += $listing->scan('profile', TRUE);
    $extensions += $listing->scan('theme', TRUE);

    foreach ($extensions as $extension) {

      $extension_name = $extension->getName();

      // Do not check libraries for disabled extensions.
      if ($extension->getType() == 'theme' && !$theme_handler->themeExists($extension_name)) {
        continue;
      }
      elseif (!$module_handler->moduleExists($extension_name)) {
        continue;
      }

      $libraries = $discovery->getLibrariesByExtension($extension_name);
      foreach ($libraries as $library_name => $library) {

        $js_data = [];
        foreach ($library['js'] as $js) {
          $js_data[] = $js['data'];
        }

        $css_data = [];
        foreach ($library['css'] as $css) {
          $css_data[] = $css['data'];
        }

        $data[] = [
          'name' => $library_name,
          'extension' => $extension_name,
          'version' => isset($library['version']) ? $library['version'] : NULL,
          'license' => isset($library['license']['name']) ? $library['license']['name'] : NULL,
          'dependencies' => implode(', ', $library['dependencies']),
          'js' => implode(', ', $js_data),
          'css' => implode(', ', $css_data),
        ];
      }
    }

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function schema() {
    return [
      'description' => 'Libraries ',
      'fields' => [
        'extension' => [
          'type' => 'varchar',
          'length' => 128,
          'not null' => TRUE,
          'default' => '',
          'description' => 'Library extension.',
        ],
        'name' => [
          'type' => 'varchar',
          'length' => 128,
          'not null' => TRUE,
          'default' => '',
          'description' => 'Library name.',
        ],
        'version' => [
          'type' => 'varchar',
          'length' => 128,
          'description' => 'Library version.',
        ],
        'license' => [
          'type' => 'varchar',
          'length' => 128,
          'description' => 'Library license.',
        ],
        'js' => [
          'type' => 'text',
          'description' => 'Library javascripts.',
        ],
        'css' => [
          'type' => 'text',
          'description' => 'Library stylesheets.',
        ],
        'dependencies' => [
          'type' => 'text',
          'description' => 'Library dependencies.',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function viewsData() {
    $data['table']['group'] = t('Libraries');
    $data['table']['base'] = [
      'field' => 'name',
      'title' => t('Libraries'),
      'help' => t('Contains a Drupal libraries.'),
    ];
    $data['extension'] = $this->viewsStandardFeld(t('Extension'), t('Library extension.'));
    $data['name'] = $this->viewsStandardFeld(t('Name'), t('Library name.'));
    $data['version'] = $this->viewsStandardFeld(t('Version'), t('Library version.'));
    $data['license'] = $this->viewsStandardFeld(t('license'), t('Library license.'));
    $data['js'] = $this->viewsStandardFeld(t('JS'), t('Library js.'));
    $data['css'] = $this->viewsStandardFeld(t('CSS'), t('Library css.'));
    $data['dependencies'] = $this->viewsStandardFeld(t('Dependencies'), t('Library dependencies.'));

    return $data;
  }

}
