<?php

/**
 * @file
 * Contains \Drupal\development_reports\Plugin\DevelopmentReport\Plugins.
 */

namespace Drupal\development_reports\Plugin\DevelopmentReport;

use Drupal\development_reports\DevelopmentReportPluginBase;

/**
 * Plugin implementation of the 'Services' development report.
 *
 * @DevelopmentReport(
 *   id = "plugins",
 *   label = @Translation("Plugins"),
 *   description = @Translation("List of Drupal plugins.")
 * )
 */
class Plugins extends DevelopmentReportPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function collect() {

    /** @var \Drupal\Core\DependencyInjection\ContainerBuilder $container */
    $container = \Drupal::getContainer();
    foreach ($container->getServiceIds() as $service_id) {
      if (strpos($service_id, 'plugin.manager') === 0 || $service_id == 'typed_data_manager') {
        $definitions = \Drupal::service($service_id)->getDefinitions();
        foreach ($definitions as $id => $definition) {
          $data[] = [
            'id' => $id,
            'provider' => isset($definition['provider']) ? $definition['provider'] : '',
            'manager' => str_replace('plugin.manager.', '', $service_id),
            'class' => $definition['class'],
          ];
        }
      }
    }

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function schema() {
    return [
      'description' => 'Plugins',
      'fields' => [
        'id' => [
          'type' => 'varchar',
          'length' => 128,
          'not null' => TRUE,
          'default' => '',
          'description' => 'Plugin ID.',
        ],
        'provider' => [
          'type' => 'varchar',
          'length' => 128,
          'not null' => TRUE,
          'default' => '',
          'description' => 'Plugin provider.',
        ],
        'manager' => [
          'type' => 'varchar',
          'length' => 128,
          'not null' => TRUE,
          'default' => '',
          'description' => 'Plugin manager.',
        ],
        'class' => [
          'type' => 'varchar',
          'length' => 128,
          'not null' => TRUE,
          'default' => '',
          'description' => 'Plugin class.',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function viewsData() {
    $data['table']['group'] = t('Plugins');
    $data['table']['base'] = [
      'field' => 'path',
      'title' => t('Plugins'),
      'help' => t('Contains a list of Drupal plugins.'),
    ];
    $data['id'] = $this->viewsStandardFeld(t('ID'), t('Plugin ID.'));
    $data['provider'] = $this->viewsStandardFeld(t('Provider'), t('Plugin provider.'));
    $data['manager'] = $this->viewsStandardFeld(t('Manager'), t('Plugin manager.'));
    $data['class'] = $this->viewsStandardFeld(t('Class'), t('Plugin class.'));
    return $data;
  }

}
