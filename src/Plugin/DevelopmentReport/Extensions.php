<?php

/**
 * @file
 * Contains \Drupal\development_reports\Plugin\DevelopmentReport\Extensions.
 */

namespace Drupal\development_reports\Plugin\DevelopmentReport;

use Drupal\development_reports\DevelopmentReportPluginBase;
use Drupal\Core\Extension\ExtensionDiscovery;

/**
 * Plugin implementation of the 'Libraries' development report.
 *
 * @DevelopmentReport(
 *   id = "extensions",
 *   label = @Translation("Extensions"),
 *   description = @Translation("List of Drupal extensions.")
 * )
 */
class Extensions extends DevelopmentReportPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function collect() {

    $listing = new ExtensionDiscovery(DRUPAL_ROOT);
    $listing->setProfileDirectories(array());
    $extensions = $listing->scan('module', TRUE);
    $extensions += $listing->scan('profile', TRUE);
    $extensions += $listing->scan('theme', TRUE);


    $module_handler = \Drupal::service('module_handler');
    $theme_handler = \Drupal::service('theme_handler');

    foreach ($extensions as $extension) {
      $enabled = $extension->getType() == 'theme' ?
        $theme_handler->themeExists($extension->getName()) :
        $module_handler->moduleExists($extension->getName());

      $data[] = [
        'name' => $extension->getName(),
        'type' => $extension->getType(),
        'path' => $extension->getPath(),
        'status' => (int) $enabled,
      ];
    }

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function schema() {
    return [
      'description' => 'Extensions ',
      'fields' => [
        'name' => [
          'type' => 'varchar',
          'length' => 128,
          'not null' => TRUE,
          'default' => '',
          'description' => 'Name fof the extension',
        ],
        'type' => [
          'type' => 'varchar',
          'length' => 128,
          'not null' => TRUE,
          'default' => '',
          'description' => 'Type of the extension',
        ],
        'path' => [
          'type' => 'varchar',
          'length' => 128,
          'description' => 'Path to the extension',
        ],
        'status' => [
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 0,
          'size' => 'tiny',
          'description' => 'Boolean indicated wheather the extension is enabled.',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function viewsData() {
    $data['table']['group'] = t('Extension');
    $data['table']['base'] = [
      'field' => 'name',
      'title' => t('Extensions'),
      'help' => t('Contains Drupal Extensions.'),
    ];
    $data['name'] = $this->viewsStandardFeld(t('Name'), t('Extension name.'));
    $data['type'] = $this->viewsStandardFeld(t('Type'), t('Extension type.'));
    $data['path'] = $this->viewsStandardFeld(t('Path'), t('Extension path.'));
    $data['status'] = $this->viewsBooleanField(
      t('Status'),
      t('Whether the extension is enabled.'),
      t('Enabled')
    );

    return $data;
  }

}
