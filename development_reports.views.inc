<?php

/**
 * @file
 * Provide views data for development_reports module.
 */

/**
 * Implements hook_views_data().
 */
function development_reports_views_data() {
  $plugin_manager = \Drupal::service('plugin.manager.development_report');
  foreach ($plugin_manager->getDefinitions() as $definition) {
    $plugin = $plugin_manager->createInstance($definition['id'], []);
    $data['development_reports_' . $definition['id']] = $plugin->viewsData();
  };

  return $data;
}
